import { IfStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formcomponent',
  templateUrl: './formcomponent.component.html',
  styleUrls: ['./formcomponent.component.css']
})
export class FormcomponentComponent implements OnInit {
  nif:string = '';
  
  constructor() { }
  
  ngOnInit(): void {
  }
  setDni():boolean
  {   
    const nifRexp :any = /^[XYZ]?([0-9]{7,8})([A-Z])$/i;
    let ok = false
    if (!nifRexp.test(this.nif))
    {
        alert("no es un nif");
    }else
    {  
       if(this.dataController(this.nif))
       {
         alert("validación correcta");
         ok = true;
       }
    }
    return ok;
  }
  dataController(nie:string):boolean
  {
      const firstNumber:number = parseInt(nie.charAt(0));
      let bok = false;
      if(isNaN(firstNumber))
      {        
        const nieWithoutFirstLetter = this.validateNumberNieControl(nie);        
        if(this.validateNumberNifControl(nieWithoutFirstLetter))
        {
           bok = true;
        }else
        {
          alert("nie incorrecto");
        }
      }else
      {
        if(this.validateNumberNifControl(nie))
        {
           bok = true;
        }else
        {
          alert(" NIf incorrecto")
        }
      }
      return bok;
  }
  validateNumberNieControl(nie:string):string
  {
    const letraNie = nie.charAt(0).toUpperCase();  
    let nieReplace:string = '';
    if(letraNie.charAt(0) == 'X')
    {
      nieReplace = nie.replace('X','0');     
    }else if(letraNie.charAt(0) == 'Y')
    {
      nieReplace = nie.replace('Y','1');

    }else if(letraNie.charAt(0) == 'Z')
    {
      nieReplace = nie.replace('Z','2');
    }
    return nieReplace;
  }
  validateNumberNifControl(nif:string):boolean
  {
    let bok = false;
    let numero = nif.substr(0,nif.length-1);
    let numberInteger:number = parseInt(numero);
    let letranif = nif.substr(nif.length-1,1).toUpperCase();
    numberInteger = numberInteger % 23;
    let letras='TRWAGMYFPDXBNJZSQVHLCKET';
    letras = letras.substring(numberInteger,numberInteger+1);
    if(letranif == letras)
    {     
      bok = true;
    }
    return bok;
  }

}
